package com.example.wheelsdemo


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.wheelsdemo.databinding.ActivityMainBinding
import com.example.wheelsdemo.databinding.ActivityPerfilUsuarioBinding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import java.util.*
import com.example.wheelsdemo.singletons.FirebaseSingleton
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.*


class MainActivity : AppCompatActivity() {

    val db = Firebase.firestore
    lateinit var sharedPref:SharedPreferences

    /*
    Autenticacion con Google
    */
    // [START declare_auth]
    companion object {
        lateinit var auth: FirebaseAuth
        private const val TAG = "GoogleActivity"
        private const val RC_SIGN_IN = 9001
    }

    // [END declare_auth]

    private lateinit var googleSignInClient: GoogleSignInClient
    private var view: View? = null
    lateinit var binding2:ActivityPerfilUsuarioBinding
    lateinit var binding:ActivityMainBinding
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics

        sharedPref = getSharedPreferences("sharedPref", MODE_PRIVATE)

        binding2 = ActivityPerfilUsuarioBinding.inflate(layoutInflater)

        // Codigo temporal notificaciones
        Firebase.messaging.subscribeToTopic("new_user_forums")

        val login = binding.logginButton

        auth = FirebaseAuth.getInstance()


        login.setOnClickListener {
            view = it
            // [START config_signin]
            // Configure Google Sign In
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id_2))
                .requestEmail()
                .build()

            googleSignInClient = GoogleSignIn.getClient(this, gso)
            // [END config_signin]

            signIn()
        }
        binding.pruebaButton.setOnClickListener {
            //throw RuntimeException("Test Crash") // Force a crash
            var perfilIntent = Intent(this, PerfilUsuarioActivity::class.java)
            if(perfilIntent.resolveActivity(packageManager)!=null){
                startActivity(perfilIntent)
            }

        }
    }

    // [START on_start_check_user]
    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        var currentUser = auth.currentUser
    }
    // [END on_start_check_user]

    // [START onactivityresult]
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == FirebaseSingleton.RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                Log.d(FirebaseSingleton.TAG, "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(FirebaseSingleton.TAG, "Google sign in failed", e)
                updateUI(null)
            }
        }

    }
    // [END onactivityresult]

    // [START auth_with_google]
    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {

                    // Si el usuario después de este gonorréico montón de pasos logra loguearse, ponga
                    // que ya está logueado.
                    val initInformation = getSharedPreferences("InitInformation", Context.MODE_PRIVATE)
                    with(initInformation.edit()) {
                        putBoolean("loggedIn", true)
                        apply()
                    }

                    // Sign in success, update UI with the signed-in user's information
                    Log.d(FirebaseSingleton.TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user?.uid)
                    val externalScope: CoroutineScope = GlobalScope
                    externalScope.launch {
                        withContext(Dispatchers.IO){
                            loginUser(user)
                        }
                    }
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(FirebaseSingleton.TAG, "signInWithCredential:failure", task.exception)
                    updateUI(null)
                }
            }
    }
    // [END auth_with_google]

    // [START signin]
    private fun signIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, FirebaseSingleton.RC_SIGN_IN)
    }
    // [END signin]

    private fun updateUI(user: String?) {
        user?.let {
            startActivity(Intent(this, SearchTripActivity::class.java))
        }?:run{
            var errorIntent = Intent(this, ErrorActivity::class.java)
            if(errorIntent.resolveActivity(packageManager)!=null){
                startActivity(errorIntent)
            }
        }
    }

    private fun loginUser(authUser: FirebaseUser?){
        // Create a new user with a first and last name
        var calendar = Calendar.getInstance()
        var day = calendar.get(Calendar.DAY_OF_WEEK)

        val user = hashMapOf(
            "name" to authUser?.displayName,
            "email" to authUser?.email,
            "day" to day,
            "avg" to 1300
        )

        //createUser(authUser)
        // .document(authUser?.tenantId.toString())
        db
            .collection("users")
            .document(authUser?.email.toString())
            .get()
            .addOnSuccessListener { doc->
                // Usuario existe
                if(doc.exists()) {
                    Log.d(TAG, "Usuario ya existente ingresado: ${authUser?.email}")
                } else {
                    createUser(authUser)
                    Log.d(TAG, "Usuario creado: ${authUser?.email}")
                }
            }
            .addOnFailureListener {
                createUser(authUser)
                Log.d(TAG, "Usuario creado: ${authUser?.email}")
            }
    }

    private fun createUser(authUser: FirebaseUser?) {
        // Create a new user with a first and last name
        var calendar = Calendar.getInstance()
        var day = calendar.get(Calendar.DAY_OF_WEEK)
        var name = authUser?.displayName
        var email = "email" to authUser?.email
        var phone = authUser?.phoneNumber
        var image = authUser?.photoUrl
        val user = hashMapOf(
            "uid" to authUser?.uid,
            "name" to name,
            "email" to email,
            "phone" to phone,
            "encodedImage" to image,
            "day" to day,
            "avg" to 1300
        )


        with(sharedPref.edit()) {
            putString("name", name)
            putString("number", phone)
            apply()
        }

        // Add a new document with a generated ID
        // authUser?.email.toString()
        FirebaseSingleton.db.collection("users").document(authUser?.email.toString()).set(user)
            .addOnSuccessListener { eo ->
                Log.d(FirebaseSingleton.TAG, "DocumentSnapshot added with ID: ${authUser?.email}")
            }
            .addOnFailureListener { e ->
                Log.w(FirebaseSingleton.TAG, "Error adding document", e)
            }
    }

}