package com.example.wheelsdemo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wheelsdemo.databinding.ActivityPasajerosBinding
import com.example.wheelsdemo.databinding.ActivityTripInformationBinding
import com.example.wheelsdemo.singletons.FirebaseSingleton.Companion.db
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_pasajeros.*
import kotlinx.coroutines.*

class PasajerosActivity : AppCompatActivity() {

    lateinit var binding: ActivityPasajerosBinding
    private val db = Firebase.firestore
    private val collectionReference: CollectionReference = db.collection("users2");

    var pasajeroAdapter: PasajeroAdapter? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPasajerosBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpRecyclerview()

    }

    fun setUpRecyclerview(){

        val query : Query = collectionReference;
        val firestoreRecyclerOptions: FirestoreRecyclerOptions<PasajerosModel> = FirestoreRecyclerOptions.Builder<PasajerosModel>()
            .setQuery(query, PasajerosModel::class.java)
            .build();

        pasajeroAdapter = PasajeroAdapter(firestoreRecyclerOptions);

        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = pasajeroAdapter

    }

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(ConnectivityManager
                .EXTRA_NO_CONNECTIVITY, false)
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
    }

    private fun disconnected() {
        recyclerView.visibility = View.GONE
        binding.errorImageView2.visibility = View.VISIBLE
        binding.errorTextView2.visibility = View.VISIBLE
    }

    private fun connected() {
        recyclerView.visibility = View.VISIBLE
        binding.errorImageView2.visibility = View.GONE
        binding.errorTextView2.visibility = View.GONE
    }

    override fun onStart() {
        super.onStart()

        // global Scope, para que
        val externalScope: CoroutineScope = GlobalScope
        externalScope.launch {

            withContext(Dispatchers.IO){
                // As this instruction is a firebase firestore listener,
                // and when changes are detected, it is incharge for filling the adapter
                // this is an input output operation
                pasajeroAdapter!!.startListening()

                registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        pasajeroAdapter!!.stopListening()
        unregisterReceiver(broadcastReceiver)
    }




}