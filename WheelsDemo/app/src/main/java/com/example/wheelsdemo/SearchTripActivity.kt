package com.example.wheelsdemo

import android.Manifest
import android.content.Context
import android.app.Activity
import android.net.ConnectivityManager
import android.content.Intent
import android.net.NetworkCapabilities
import android.view.View
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.TypedValue
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.wheelsdemo.singletons.FirebaseSingleton
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.location.FusedLocationProviderClient
import android.location.Location
import com.example.wheelsdemo.databinding.ActivityPerfilUsuarioBinding
import com.example.wheelsdemo.databinding.ActivitySearchTripBinding
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import java.util.*

class SearchTripActivity : AppCompatActivity() {

    // Constants

    val AUTOCOMPLETE_REQUEST_CODE = 1

    /*
    Variable that stores the Uniandes Coordinates, because user can only either to or from Uniandes.
     */
    val uniandesCoordinates = "4.6031,-74.0656"

    /*
    First coordinate of the bias of the Maps fragment.
     */
    val southWest = LatLng(4.512, -74.313)

    /*
    Second coordinate of the bias of the Maps fragment.
     */
    val northEast = LatLng(4.905, -73.954)

    // Variables

    /*
    Variable that stores if the user selected to be a passenger.
    If false, it selected driver.
     */
    var passenger = true

    /*
    Variable that stores if the user selected to go to Uniandes.
    If false, it selected to go from Uniandes.
     */
    var toU = true

    /*
    Variable that stores the name of the place chosen by the user.
    By default they are empty string.
     */
    var placeName = ""

    /*
    Variable that stores the coordinates of the place chosen by the user.
    By default they are empty string.
     */
    var otherCoordinates = ""

    /*
    Variable that stores the price that the user selected.
    By default it is 4k pesos.
     */
    var price = 4000

    /*
    Variable that stores the number of seats that a driver selected.
    By default it is 4 seats.
     */
    var seats = 4

    lateinit var datePicker: DatePicker

    lateinit var timePicker: TimePicker

    lateinit var fusedLocationClient: FusedLocationProviderClient

    lateinit var placesText : TextView

    lateinit var locationRequest: LocationRequest

    lateinit var radioButtonToHouse: RadioButton

    lateinit var binding: ActivitySearchTripBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchTripBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        val initInformation = getSharedPreferences("InitInformation", Context.MODE_PRIVATE)
        val hasTrip = initInformation.getBoolean("hasTrip", false)
        if(hasTrip) {
            startActivity(Intent(this, TripInformationActivity::class.java))
        }

        val userButtons = typeOfUserBehaviour()
        val autocompleteFragment = searchBarBehaviour()
        val tripButtons = typeOfTripBehaviour()
        submitButtonBehaviour()

        val sharedPreferences = getPreferences(Context.MODE_PRIVATE)
        placeName = sharedPreferences.getString("PLACE_NAME", placeName)!!
        otherCoordinates = sharedPreferences.getString("OTHER_COORDINATES", otherCoordinates)!!
        passenger = sharedPreferences.getBoolean("PASSENGER", passenger)
        toU = sharedPreferences.getBoolean("TO_U", toU)

        userButtons[if (passenger) 0 else 1].performClick()

        if(isLocationPermissionGranted()) {
            getLastLocation()
        } else {
            tripButtons[if (toU) 0 else 1].performClick()
        }

        placesText.setText(placeName)

        loadCache()
    }

    fun getLastLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                if(location != null) {
                    val uniandesArray = uniandesCoordinates.split(",")
                    if(Math.abs(location.latitude - uniandesArray[0].toFloat()) <= 0.004
                        && Math.abs(location.longitude - uniandesArray[1].toFloat()) <= 0.004) {
                        radioButtonToHouse.performClick()
                    }
                }
            }
    }

    fun isLocationPermissionGranted(): Boolean {
        var granted = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        if(!granted) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MapsActivity.PERMISSION_REQUEST_ACCESS_FINE_LOCATION)
        }
        return granted
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == MapsActivity.PERMISSION_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                showAlert("Los permisos de ubicación son necesarios para el correcto funcionamiento de la aplicación",
                    "Error")
            } else {
                getLastLocation()
            }
        }
    }

    fun loadCache() {
        val sharedPreferences = getPreferences(Context.MODE_PRIVATE)
        price = sharedPreferences.getInt("PRICE", price)
        seats = sharedPreferences.getInt("SEATS", seats)
        val editTextPrice = findViewById<TextView>(R.id.editTextPrice)
        val editTextPassengers = findViewById<TextView>(R.id.editTextPassengers)
        editTextPrice.text = price.toString()
        editTextPassengers.text = seats.toString()
    }

    fun typeOfUserBehaviour(): Array<RadioButton> {
        val textViewPrice = binding.textViewPrice
        val editTextPrice = findViewById<TextView>(R.id.editTextPrice)
        val textViewPassengers = findViewById<TextView>(R.id.textViewPassengers)
        val editTextPassengers = findViewById<TextView>(R.id.editTextPassengers)
        val textViewDate = findViewById<TextView>(R.id.textViewDate)
        datePicker = findViewById<DatePicker>(R.id.date_picker)
        datePicker.setMinDate(System.currentTimeMillis())
        // Máximo una semana después
        datePicker.setMaxDate(System.currentTimeMillis() + 604800000)
        val textViewTime = findViewById<TextView>(R.id.textViewTime)
        timePicker = findViewById<TimePicker>(R.id.time_picker)

        editTextPrice.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if(s != null && s.length > 0) {
                    val offeredPrice = Integer.parseInt(s.toString())
                    if(offeredPrice == 0) {
                        showAlert("Cuidado, no vas a cobrar", "Información")
                        price = 0
                    } else if(offeredPrice > 20000) {
                        showAlert("No puedes cobrar tanto", "Error")
                        s.replace(0, s.length, "20000")
                        price = 15
                    } else {
                        price = offeredPrice
                    }
                } else {
                    price = 4000
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        editTextPassengers.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if(s != null && s.length > 0) {
                    val offeredSeats = Integer.parseInt(s.toString())
                    if(offeredSeats == 0) {
                        showAlert("No puedes no tener sillas", "Error")
                        s.replace(0, s.length, "1")
                        seats = 1
                    } else if(offeredSeats > 15) {
                        showAlert("No puedes tener tantas sillas", "Error")
                        s.replace(0, s.length, "15")
                        seats = 15
                    } else {
                        seats = offeredSeats
                    }
                } else {
                    seats = 4
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        val radioButtonPassenger = binding.radioButtonPassenger
        radioButtonPassenger.setOnClickListener {
            passenger = true
            textViewPrice.text = "Máximo precio que quiere pagar"
            textViewPassengers.visibility = View.GONE
            editTextPassengers.visibility = View.GONE
            textViewDate.visibility = View.GONE
            datePicker.visibility = View.GONE
            textViewTime.visibility = View.GONE
            timePicker.visibility = View.GONE
            title = "Pedir un viaje"
        }

        val radioButtonDriver = binding.radioButtonDriver
        radioButtonDriver.setOnClickListener {
            passenger = false
            textViewPrice.text = "Precio por silla"
            textViewPassengers.visibility = View.VISIBLE
            editTextPassengers.visibility = View.VISIBLE
            textViewDate.visibility = View.VISIBLE
            datePicker.visibility = View.VISIBLE
            textViewTime.visibility = View.VISIBLE
            timePicker.visibility = View.VISIBLE
            title = "Ofrecer un viaje"
        }

        return arrayOf(radioButtonPassenger, radioButtonDriver)
    }

    fun searchBarBehaviour() {
        Places.initialize(applicationContext, "AIzaSyDuteHT7H0cOTCrVPPtFrYBa8f4cKplDpA")
        val placesClient = Places.createClient(this)
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        val fields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)

        placesText = binding.placesText
        placesText.setOnClickListener {
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                .setLocationRestriction(RectangularBounds.newInstance(southWest, northEast))
                .build(this)
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
        }

        val placesButton = binding.placesButton
        placesButton.setOnClickListener {
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                .setLocationRestriction(RectangularBounds.newInstance(southWest, northEast))
                .build(this)
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        if(place.latLng != null) {
                            val latitude = Math.round(place.latLng!!.latitude * 100000.0) / 100000.0
                            val longitude = Math.round(place.latLng!!.longitude * 100000.0) / 100000.0
                            otherCoordinates = "$latitude,$longitude"
                            placeName = place.name
                            placesText.setText(placeName)
                        }
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        showAlert(status.statusMessage ?: "", "Info")
                    }
                }
                Activity.RESULT_CANCELED -> {
                    if(otherCoordinates == "" && placeName == "")
                    showAlert("Tienes que ingresar una dirección", "Error")
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun typeOfTripBehaviour(): Array<RadioButton> {
        val textViewUFrom = binding.textViewUFrom
        val textViewUTo = binding.textViewUTo
        val textViewToHouse = binding.textViewToHouse
        val textViewToU = binding.textViewToU
        val dp48 =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48f, resources.displayMetrics)
                .toInt()

        val radioButtonToU = binding.radioButtonToU
        radioButtonToU.setOnClickListener {
            toU = true
            textViewUFrom.visibility = View.GONE
            textViewToHouse.visibility = View.GONE
            textViewToU.visibility = View.VISIBLE
            textViewUTo.visibility = View.VISIBLE
        }

        radioButtonToHouse = binding.radioButtonToHouse
        radioButtonToHouse.setOnClickListener {
            toU = false
            textViewUFrom.visibility = View.VISIBLE
            textViewToHouse.visibility = View.VISIBLE
            textViewToU.visibility = View.GONE
            textViewUTo.visibility = View.GONE
        }

        return arrayOf(radioButtonToU, radioButtonToHouse)
    }

    fun databaseCollection(passenger: Boolean): String {
        return if (passenger) "requests" else "offers"
    }

    fun submitButtonBehaviour() {
        val submitButton = binding.submitButton
        submitButton.setOnClickListener {
            if (otherCoordinates == "") {
                showAlert("Necesitas ingresar una dirección", "Error")
            } else {
                submitActivity()
            }
        }
    }

    fun submitActivity() {
        if (!isOnline(applicationContext)) {
            showAlert("No tienes conexión", "Error")
            return
        }
        val request: HashMap<String, Any> = hashMapOf(
            "uid" to MainActivity.auth.currentUser!!.uid,
            "submitTime" to Calendar.getInstance().time,
            "otherAddressName" to placeName,
            "toU" to toU,
            "otherAddress" to if (toU) otherCoordinates else uniandesCoordinates
        )
        if (passenger) createPassengerIntent(request) else createDriverIntent(request)
    }

    fun createPassengerIntent(request: HashMap<String, Any>) {
        val coordinatesIntent = Intent(this, MapsActivity::class.java);
        coordinatesIntent.putExtra("coordinates", otherCoordinates)
        request.put("maxPrice", price)
        FirebaseSingleton.db.collection("requests").add(request)
            .addOnSuccessListener { eo ->
                startActivity(coordinatesIntent)
            }
            .addOnFailureListener { e ->
                showAlert(e.toString(), "Error")
            }
    }

    /**
     * Aca se invoca la actividad, cuando el viaje ya ha sido creado
     */
    fun createDriverIntent(request: HashMap<String, Any>) {
        val driverIntent = Intent(this, TripInformationActivity::class.java);
        val dateTimeCreator: Calendar = Calendar.Builder()
            .setDate(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth())
            .setTimeOfDay(timePicker.getHour(), timePicker.getMinute(), 0)
            .build();

        if (dateTimeCreator.time < Calendar.getInstance().time) {
            showAlert("Debes ingresar una hora mayor a la actual", "Error")
            return
        }

        request.put("pricePerSeat", price)
        request.put("seats", seats)
        request.put("tripDateTime", dateTimeCreator.time)

        FirebaseSingleton.db.collection("offers").add(request)
            .addOnSuccessListener { eo ->
                startActivity(driverIntent)
            }
            .addOnFailureListener { e ->
                showAlert(e.toString(), "Error")
            }
    }

    /*
    Function that handles the activity lifecycle when the user stops using the app.
     */
    override fun onPause() {
        super.onPause()
        storeVariables()
    }

    /*
    Function that stores the variables in the Shared Preferences of the activity.
     */
    fun storeVariables() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString("PLACE_NAME", placeName)
            putString("OTHER_COORDINATES", otherCoordinates)
            putBoolean("PASSENGER", passenger)
            putBoolean("TO_U", toU)
            putInt("PRICE", price)
            putInt("SEATS", seats)
            apply()
        }
    }

    fun showAlert(message:String, title:String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(message)
        val dialog = builder.create()
        dialog.show()
    }

    companion object {
        fun isOnline(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivityManager != null) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                        return true
                    }
                }
            }
            return false
        }
    }
}