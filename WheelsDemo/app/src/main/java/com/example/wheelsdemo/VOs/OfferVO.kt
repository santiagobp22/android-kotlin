package com.example.wheelsdemo.VOs

import com.google.firebase.Timestamp
import java.util.*

data class OfferVO(
    val uid: String = "",
    val otherAddress: String = "",
    val otherAddressName: String = "",
    val pricePerSeat: Int = 0,
    val seats: Int = 0,
    val submitTime: Timestamp = Timestamp(Calendar.getInstance().time),
    val toU: Boolean = true,
    val tripDateTime: Timestamp = Timestamp(0L, 0)
) {
}