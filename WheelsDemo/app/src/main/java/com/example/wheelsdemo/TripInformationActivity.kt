package com.example.wheelsdemo

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.util.ArrayMap
import kotlinx.coroutines.launch
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.wheelsdemo.VOs.OfferVO
import com.example.wheelsdemo.databinding.ActivityPerfilUsuarioBinding
import com.example.wheelsdemo.databinding.ActivityTripInformationBinding
import com.example.wheelsdemo.singletons.FirebaseSingleton
import com.google.firebase.Timestamp
import kotlinx.android.synthetic.main.activity_trip_information.*

class TripInformationActivity : AppCompatActivity() {

    lateinit var tripId: String
    lateinit var binding:ActivityTripInformationBinding

    var tripData: ArrayMap<String, OfferVO> = ArrayMap()
    // En el caso de ser para varios trips, sería tipo ArrayMap<String, ArrayMap<String, Any>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTripInformationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sharedPreferences = getSharedPreferences("InitInformation", Context.MODE_PRIVATE)
        with(sharedPreferences.edit()) {
            putBoolean("hasTrip", true)
            apply()
        }

        title = "Información del viaje"

        lifecycleScope.launch(Dispatchers.IO) {
            getTripInformation()
        }

        goTripButton()
        cancelTripButton()

        binding.pasajerosButton.setOnClickListener {
            var pasajerosIntent = Intent(this, PasajerosActivity::class.java)
            if(pasajerosIntent.resolveActivity(packageManager)!=null){
                startActivity(pasajerosIntent)
            }
            traerUsuarios()
        }
    }

    // Boton pasajeros
    fun traerUsuarios(){

    }




    /*
    Function that gets the trip information from database.
    TENGO QUE HACERLA ASYNC.
     */
    suspend fun getTripInformation() {
        val salidaDireccion = findViewById<TextView>(R.id.salidaDireccion)
        val llegadaDireccion = findViewById<TextView>(R.id.llegadaDireccion)
        val hora = findViewById<TextView>(R.id.hora)
        val conductor = findViewById<TextView>(R.id.conductor)

        FirebaseSingleton.db.collection("offers").whereEqualTo("uid", MainActivity.auth.currentUser!!.uid).get()
            .addOnSuccessListener { documents ->
                for(document in documents) {
                    tripId = document.id
                    val trip = document.toObject(OfferVO::class.java)

                    tripData.put(tripId, trip)

                    salidaDireccion.text = if(trip.toU) trip.otherAddressName else "Universidad de los Andes"
                    llegadaDireccion.text = if(trip.toU) "Universidad de los Andes" else trip.otherAddressName
                    hora.text = trip.tripDateTime.toDate().toString()
                    conductor.text = trip.uid
                }
            }
            .addOnFailureListener { e ->
                showAlert("No tienes conexión", "Error")
                if(!tripData.isEmpty()) {
                    val trip = tripData.get(tripId) ?: tripData.valueAt(0)
                    salidaDireccion.text = if(trip.toU) trip.otherAddressName else "Universidad de los Andes"
                    llegadaDireccion.text = if(trip.toU) "Universidad de los Andes" else trip.otherAddressName
                    hora.text = trip.tripDateTime.toDate().toString()
                    conductor.text = trip.uid
                }
            }
    }

    /*
    Method that describes the functionality of the go to trip button.
     */
    fun goTripButton() {
        val button = findViewById<Button>(R.id.goTrip)
        button.setOnClickListener {
            if(SearchTripActivity.isOnline(applicationContext)) {
                // DANIEL, USE THIS
            } else {
                showAlert("No tienes conexión", "Error")
            }
        }
    }

    /*
    Method that describes the functionality of the cancel trip button.
     */
    fun cancelTripButton() {
        val button = findViewById<Button>(R.id.cancelTrip)
        button.setOnClickListener {
            if(SearchTripActivity.isOnline(applicationContext)) {
                FirebaseSingleton.db.collection("offers").document(tripId).delete()
                    .addOnSuccessListener { eo ->
                        val sharedPreferences = getSharedPreferences("InitInformation", Context.MODE_PRIVATE)
                        with(sharedPreferences.edit()) {
                            putBoolean("hasTrip", false)
                            apply()
                        }
                        finish()
                    }
                    .addOnFailureListener { e ->
                        showAlert(e.toString(), "Error")
                    }
            } else {
                showAlert("No tienes conexión", "Error")
            }
        }
    }

    /*
    Function that handles the activity lifecycle when the user stops using the app.
     */
    override fun onPause() {
        super.onPause()
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString("tripId", tripId)
            apply()
        }
    }

    override fun onResume() {
        super.onResume()
        lifecycleScope.launch(Dispatchers.IO) {
            getTripInformation()
        }
    }

    override fun onRestart() {
        super.onRestart()
        lifecycleScope.launch(Dispatchers.IO) {
            getTripInformation()
        }
    }

    override fun onBackPressed() {
        showAlert("Si quieres cancelar el viaje oprime el botón \"Cancelar Viaje\"", "Error")
    }

    fun showAlert(message:String, title:String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(message)
        val dialog = builder.create()
        dialog.show()
    }
}