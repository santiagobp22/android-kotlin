package com.example.wheelsdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import kotlinx.android.synthetic.main.row_pasajeros.view.*

class PasajeroAdapter(options: FirestoreRecyclerOptions<PasajerosModel>) :


    FirestoreRecyclerAdapter<PasajerosModel, PasajeroAdapter.PasajeroAdapterVH>(options) {

    // Create new views (invoked by the layout manager)
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PasajeroAdapterVH {
        // Create a new view, which defines the UI of the list item
                return PasajeroAdapterVH(LayoutInflater.from(parent.context).inflate(R.layout.row_pasajeros,parent,false))
            }

    // Replace the contents of a view (invoked by the layout manager)
            override fun onBindViewHolder(holder: PasajeroAdapterVH, position: Int, model: PasajerosModel) {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
                holder.userName.text = model.userName;
                holder.user_email.text = model.userEmail
                holder.user_phone.text = model.userPhone
            }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
        class PasajeroAdapterVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // utiliza row_pásajeros
            var userName = itemView.tvUsername
            var user_email = itemView.tvUserEmail
            var user_phone = itemView.tvUserPhone

        }
}