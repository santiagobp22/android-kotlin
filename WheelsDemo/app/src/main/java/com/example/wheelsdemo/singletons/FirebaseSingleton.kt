package com.example.wheelsdemo.singletons

import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

/*
Singleton that represents the connection to the Firebase db.
 */
class FirebaseSingleton {
    companion object {
        val db = Firebase.firestore
        val TAG = "GoogleActivity"
        val RC_SIGN_IN = 9001
    }
}